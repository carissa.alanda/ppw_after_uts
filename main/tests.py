from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story7

class TestView(TestCase):
      
    def test_apakah_di_halaman_ada_templatenya(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/story7.html')

class TestURL(TestCase):

    def test_apakah_di_halaman_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

class TestView(TestCase):
    def setUp(self):
        self.main = reverse("main:story7")
    
    def test_apakah_ada_func_story7(self):
        found = resolve(self.main)
        self.assertEqual(found.func, story7)



'''@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
'''