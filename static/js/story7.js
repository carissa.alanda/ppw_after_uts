var acc = document.getElementsByClassName("accordion");

var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function(){ //class accordion
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
$(function () {
    $('.move-up').on('click', function (e) {
        var wrapper = $(this).closest('.box'); //class box
        wrapper.insertBefore(wrapper.prev());
        var panel = wrapper.find('.panel')[0]; //class panel
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    })
    $('.move-down').on('click', function (e) {
        var wrapper = $(this).closest('.box');
        wrapper.insertAfter(wrapper.next());
        var panel = wrapper.find('.panel')[0];
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    })
})