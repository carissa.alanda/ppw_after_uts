from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

class UnitTest(TestCase):
      
    def test_apakah_di_halaman_ada_templatenya(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'story8/books.html')

    def test_apakah_di_halaman_ada_url(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_func_index(self):
        found = resolve('/books/')
        self.assertEqual(found.func, views.index)
    
    def test_apakah_di_halaman_data_ada_url(self):
        response = Client().get('/data/?q=frozen')
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_ada_func_data(self):
        found = resolve('/data/')
        self.assertEqual(found.func, views.data)