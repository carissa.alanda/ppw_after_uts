from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def index(request):
    response = {}
    return render(request,'story8/books.html',response)

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    data = requests.get(url).json()
    return JsonResponse(data)
