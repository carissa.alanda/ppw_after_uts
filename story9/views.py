from django.shortcuts import render, redirect
from .forms import LoginForm, SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages

def signUp(request):
    
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("login")

        else:
            for msg in form.error_messages:
                messages.error(request, 'Invalid entry')

    else:
        form = SignUpForm()

    context = {
        'form' : form
    }

    return render(request, "story9/signup.html", context)

def login_view(request):
    
    if request.method == "POST":
        form = LoginForm(data = request.POST)

        if form.is_valid():
            usernameInput = request.POST["username"]
            passwordInput = request.POST["password"]

            user = authenticate(request, username = usernameInput, password = passwordInput)

            if user is not None:
                login(request, user)
                return redirect('login')

        else:
            messages.error(request, 'Invalid entry')

    else:
        form = LoginForm()

    context = {
        "form" : form,
    }

    return render(request, "story9/login.html", context)



@login_required
def logout_view(request):
    logout(request)
    form = LoginForm()
    response = {'form':form}
    return redirect ('login')




